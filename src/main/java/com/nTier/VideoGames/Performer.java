package com.nTier.VideoGames;

class Performer implements Audition{
	
//Attributes	
private int unionId;
private String performanceType;

//Constructor
Performer(int unionId)
{
	this.setUnionId(unionId);
	this.setPerformanceType("Performer");
}

//Getters and Setters
 int getUnionId() {
	return unionId;
}
private void setUnionId(int unionId) {
	this.unionId = unionId;
}
 String getPerformanceType() {
	return performanceType;
}
 void setPerformanceType(String performanceType) {
	this.performanceType = performanceType;
}

public String Announce() {
	return(this.getUnionId()+" - "+this.getPerformanceType());
	
}

}
