package com.nTier.VideoGames;

import java.util.HashSet;

import org.apache.log4j.Logger;

class AuditionClient {
public static void main(String args[])
{
	final Logger log = Logger.getLogger(AuditionClient.class);
	Performer p1=new Performer(121);
	Performer p2=new Performer(122);
	Performer p3=new Performer(123);
	Performer p4=new Performer(124);
	Performer p5=new Dancer(125,"Tap");
	Performer p6=new Dancer(126,"Salsa");
	Performer p7=new Vocalist(127,"A");
	HashSet<Performer> hs=new HashSet<Performer>();
	if(hs.add(p1)) {log.info(p1.getUnionId()+" is set for audition");};
	if(hs.add(p2)) {log.info(p2.getUnionId()+" is set for audition");};
	if(hs.add(p3)) {log.info(p3.getUnionId()+" is set for audition");};
	if(hs.add(p4)) {log.info(p4.getUnionId()+" is set for audition");};
	if(hs.add(p5)) {log.info(p5.getUnionId()+" is set for audition");};
	if(hs.add(p6)) {log.info(p6.getUnionId()+" is set for audition");};
	if(hs.add(p7)) {
		((Vocalist)p7).performWithVolume(5);
		log.info(p7.getUnionId()+" is set for audition");
		};
	for(Performer p:hs)
	{
		System.out.println(p.Announce());
	}
}
}
