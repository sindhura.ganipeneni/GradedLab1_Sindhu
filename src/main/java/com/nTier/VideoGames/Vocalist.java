package com.nTier.VideoGames;

class Vocalist extends Performer implements VocalRange{

	//Attributes
	String key;
	int Volume;

	//Constructor
	Vocalist(int unionId,String key) {
		super(unionId);
		this.setPerformanceType("Vocalist");
		this.setKey(key);
		
	}
	
	//Getters and Setters
	 String getKey() {
		return key;
	}

	private void setKey(String key) {
		this.key = key;
	}
	
	int getVolume() {
		return Volume;
	}

	void setVolume(int volume) {
		Volume = volume;
	}
	
	//Overriden Announce() from Performance
	public String Announce()
	{
		if(this.getVolume()==0)
		{
			return("I sing in the key of - "+this.getKey()+" - "+this.getUnionId());
		}
		else if ((this.Volume>=1)&&(this.getVolume()<=10))
		{
			return("I sing in the key of - "+this.getKey()+" - at the volume "+this.getVolume()+" - "+this.getUnionId());
		}
		else
		{
			throw new IllegalArgumentException("Volume cannnot be less than 1 or more than 10.Current volume given is "+this.getVolume());
		}
	}
	
	public void performWithVolume(int volume) {
		this.setVolume(volume);
	}

}
