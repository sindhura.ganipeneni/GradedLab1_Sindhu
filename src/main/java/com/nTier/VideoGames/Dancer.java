package com.nTier.VideoGames;

class Dancer extends Performer {
	
	//Attributes
	String Style;
	
	//Constructor
	Dancer(int unionId,String style) {
		super(unionId);
		this.setPerformanceType("Dancer");
		this.setStyle(style);
	}
	
	//Getters and Setters
	 String getStyle() {
		return Style;
	}

	private void setStyle(String style) {
		Style = style;
	}
	
	public String Announce()
	{
		return (this.getStyle()+ " - "+this.getUnionId()+ " - "+this.getPerformanceType());
	}
	
}
