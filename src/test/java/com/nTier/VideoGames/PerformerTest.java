package com.nTier.VideoGames;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {
	
	public Performer p1;
	
	@Before
	public void doSetup()
	{
		p1=new Performer(324);
	}

	@Test
	public void testAuditionAnnounceForPerformer() {
		assertEquals("324 - Performer", p1.Announce());
	}

}
