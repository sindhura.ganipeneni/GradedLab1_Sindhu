package com.nTier.VideoGames;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class VocalistTest {

	public Performer v1;
	
	@Rule
	public ExpectedException exception= ExpectedException.none();
	
	@Before
	public void doSetup()
	{
		v1=new Vocalist(213,"G");
	}

	@Test
	public void testAuditionForVocalist() {
		assertEquals("I sing in the key of - G - 213", v1.Announce());
	}
	
	@Test
	public void testAuditionForVocalistWithVolume()
	{
		Vocalist v=(Vocalist)v1;
		v.performWithVolume(2);
		assertEquals("I sing in the key of - G - at the volume 2 - 213", v.Announce());
	}
	
	@Test
	public void testAuditionForVocalistWithOutofrangeVolume()
	{
		Vocalist v=(Vocalist)v1;
		v.performWithVolume(12);
		exception.expect(IllegalArgumentException.class);
		String temp=v.Announce();
	}

}
