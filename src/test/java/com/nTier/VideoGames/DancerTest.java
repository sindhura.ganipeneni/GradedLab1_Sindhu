package com.nTier.VideoGames;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DancerTest {
	
	public Performer d1;
	
	@Before
	public void doSetup()
	{
		d1=new Dancer(772,"Tap");
	}

	@Test
	public void testAuditionAnnounceForDancer() {
		assertEquals("Tap - 772 - Dancer",d1.Announce());
	}

}
